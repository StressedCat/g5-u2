#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <typeinfo>
using namespace std;
#include "main.h"

class Grafo {
    private:

    public:
        Grafo (Nodo *nodo) {
            ofstream fp;

            /* se abre el archivo de texto */
            fp.open ("grafo.txt");

            /* Se coloca los parametros para el grafico */
            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;

            fp << "nullraiz [shape=point];" << endl;
            fp << "nullraiz->" << nodo->id << "[label=" << nodo->FE << "];" << endl;

            /* se va a la función para recorrer los datos y llenar el texto */
            recorrer(nodo, fp);

            fp << "}" << endl;

            /* se cierra el archivo */
            fp.close();

            /* Se genera grafo con el archivo con los parametros ingresados */
            system("dot -Tpng -ografo.png grafo.txt &");

            /* se visualiza el grafo */
            system("eog grafo.png &");
        }

        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        * los numeros se harán string para evitar errores al hacer el
        * grafico
        */
        void recorrer(Nodo *p, ofstream &fp) {
            string cadena = "\0";

            if (p != NULL) {
                if (p->izq != NULL) {
                    fp <<  p->id << "->" << p->izq->id << "[label=" << p->izq->FE << "];" << endl;
                } else {
                    cadena = p->id + "i";
                    fp << cadena << "[shape=point];" << endl;
                    fp << p->id << "->" << cadena << ";" << endl;
                }

                if (p->der != NULL) {
                    fp << p->id << "->" << p->der->id << "[label=" << p->der->FE << "];" << endl;
                } else {
                    cadena = p->id + "d";
                    fp <<  cadena << " [shape=point];" << endl;
                    fp << p->id << "->" << cadena << ";" << endl;
                }

                recorrer(p->izq, fp);
                recorrer(p->der, fp);
            }
        }
};

/* el str o proteina se hará un numero con ASCII */
int StrToASCII(string id){
  int suma = 0;
  for (int i = 0; i < id.length(); i++){
    char x = id.at(i);
    suma = suma + int(x);
  }
  return suma;
}

/* se añade un nodo al arbol, el donde con la ayuda de FE se podrá mantener
   balanceado */
void addnode(Nodo **Raiz, int num, bool BO, string id){
  Nodo *nodo = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL;

  nodo = *Raiz;
  /* 1. */
  /* Si no es nulo, existe un numero, por lo tanto no se puede agregar cosas */
  if (nodo != NULL) {
    /* 1.1 */
    /* Si el numero es menor al del info ira a izquierda */
    if (num < nodo->info) {
      addnode(&(nodo->izq), num, BO, id);
      /* 1.1.1 */
      if (BO == true) {
        /* 1.1.1.1 */
        if (nodo->FE == 1) {
          nodo->FE = 0;
          BO = false;
        }

        else if (nodo->FE == 0){
          nodo->FE = -1;
        }

        else if (nodo->FE == -1) {
          nodo1 = nodo->izq;
          /* 1.1.1.1.1 */
          /* Reestructuración del arbol */
          if (nodo1->FE <= 0) {
            /* rotación LL o II */
            nodo->izq = nodo1->der;
            nodo1->der = nodo;
            nodo->FE = 0;
            nodo = nodo1;
          }
          else{
            /* Rotacion ID o LR */
            nodo2 = nodo1->der;
            nodo->izq = nodo2->der;
            nodo2->der = nodo;
            nodo1->der = nodo2->izq;
            nodo2->izq = nodo1;
            /* 1.1.1.1.1.A */
            if (nodo2->FE == -1) {
              nodo->FE = 1;
            }
            else{
              nodo->FE = 0;
            }
            /* 1.1.1.1.1.B */
            /* 1.1.1.1.1.C */
            if (nodo2->FE == 1) {
              nodo1->FE = -1;
            }
            else{
              nodo1->FE = 0;
            }
            /* 1.1.1.1.1.D */
            nodo = nodo2;
          }
          /* 1.1.1.1.2 */
          nodo->FE = 0;
          BO = false;
        }
      }
    }
    else{
      /* Si el numero es mayor al del info ira a derecha */
      if (num > nodo->info) {
        addnode(&(nodo->der), num, BO, id);
        if (BO == true) {

          if (nodo->FE == -1) {
            nodo->FE = 0;
            BO = false;
          }

          else if (nodo->FE == 0){
            nodo->FE = 1;
          }

          else if (nodo->FE == 1){
            nodo1 = nodo->der;

            if (nodo1->FE >= 0) {
              /* rotación DD o RR */
              nodo->der = nodo1->izq;
              nodo1->izq = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            }

            else{
              /* rotación DI o RL */
              nodo2 = nodo1->izq;
              nodo->der = nodo2->izq;
              nodo2->izq = nodo;
              nodo1->izq = nodo2->der;
              nodo2->der = nodo1;
              if (nodo2->FE == 1) {
                nodo->FE = -1;
              }

              else{
                nodo->FE = 0;
              }

              if (nodo2->FE == -1) {
                nodo1->FE = 1;
              }

              else{
                nodo1->FE = 0;
              }

              nodo = nodo2;
            }

            nodo->FE = 0;
            BO = false;
          }
        }
      }

      else{
        cout << "El nodo ya se encuentra en el árbol" << endl;
      }
    }
  }

  /* caso contrario, esta vacio y se pueden agregar cosas */
  else{
    nodo = crearNodo(num, id);
    //BO = true;
  }

  *Raiz = nodo;
}

/* Se ordena al borrarse, para evitar quedar con un 0 en el aire y que
   quede en equilibrio
*/
void Restructura1(Nodo **temp, bool BO) {
  Nodo *nodo;
  Nodo *nodo1;
  Nodo *nodo2;
  nodo = *temp;

  if (BO == true) {
    if (nodo->FE == -1) {
      nodo->FE = 0;
    }
    else if (nodo->FE == 0) {
      nodo->FE = 1;
      BO = false;
    }
    else if (nodo->FE == 1) {
      nodo->der = nodo1->izq;
      nodo1->izq = nodo;
      if (nodo1->FE >= 0) {
        /* rotación DD o RR */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;

        if (nodo1->FE == 0) {
          nodo->FE = 1;
          nodo1->FE = 0;
          BO = false;
        }
        else if (nodo1->FE == 1) {
          nodo->FE = 0;
          nodo1->FE = 0;
          BO = false;
        }
        nodo = nodo1;
      }
      else{
        /* Rotación DI o RL */
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;

        if (nodo2->FE == 1) {
          nodo->FE = -1;
        }
        else{
          nodo->FE = 0;
        }

        if (nodo2->FE == -1) {
          nodo1->FE = 1;
        }
        else{
          nodo1->FE = 0;
        }

        nodo = nodo2;
        nodo2->FE = 0;
      }
    }
  }
  *temp = nodo;
}

/* Se ordena al borrarse, para evitar quedar con un 0 en el aire y que
   quede en equilibrio
*/
void Restructura2(Nodo **temp, bool BO) {
  Nodo *nodo;
  Nodo *nodo1;
  Nodo *nodo2;
  nodo = *temp;

  if (BO == true) {
    if (nodo->FE == 1) {
      nodo->FE = 0;
    }
    else if (nodo->FE == 0) {
      nodo->FE = -1;
      BO = false;
    }
    else if (nodo->FE == -1) {
      nodo1->der = nodo;
      if (nodo1->FE <= 0) {
        /* rotación II o LL */
        nodo->izq = nodo1->der;
        nodo1->der = nodo;

        if (nodo1->FE == 0) {
          nodo->FE = -1;
          nodo1->FE = 1;
          BO = false;
        }
        else if (nodo1->FE == -1) {
          nodo->FE = 0;
          nodo1->FE = 0;
          BO = false;
        }
        nodo = nodo1;
      }
      else{
        /* Rotación ID o LR */
        nodo2 = nodo1->der;
        nodo->izq = nodo2->der;
        nodo2->der = nodo;
        nodo1->der = nodo2->izq;
        nodo2->izq = nodo1;

        if (nodo2->FE == -1) {
          nodo->FE = 1;
        }
        else{
          nodo->FE = 0;
        }

        if (nodo2->FE == 1) {
          nodo1->FE = -1;
        }
        else{
          nodo1->FE = 0;
        }

        nodo = nodo2;
        nodo2->FE = 0;
      }
    }
  }
  *temp = nodo;
}

void Borrar(Nodo **aux1, Nodo **otro1, bool BO) {
  Nodo *aux;
  Nodo *otro;
  aux = *aux1;
  otro = *otro1;

  if (aux->der != NULL) {
    Borrar(&(aux->der), &(otro), BO);
    Restructura2(&(aux), BO);
  }
  else{
    otro->info = aux->info;
    aux = aux->izq;
    BO = true;
  }
  *aux1 = aux;
  *otro1 = otro;
}

/* se borrará un nodo que sea igual al ASCII de la proteina ingresada por
   el usuario */
bool delnode(Nodo **Raiz, bool BO, int num){
  Nodo *nodo;
  Nodo *temp;

  nodo = *Raiz;

  if (nodo != NULL) {
    if (num < nodo->info) {
      delnode(&(nodo->izq), BO, num);
      Restructura1(&(nodo), BO);
    }
    else{
      if (num > nodo->info) {
        delnode(&(nodo->der), BO, num);
        Restructura2(&(nodo), BO);
      }
      else{
        temp = nodo;
        if (temp->der == NULL) {
          nodo = temp->izq;
          BO = true;
        }
        else{
          if (temp->izq == NULL) {
            nodo = temp -> der;
            BO = true;
          }
          else{
            if (temp->izq == NULL) {
              nodo = temp->der;
              BO = true;
            }
            else{
              Borrar(&(temp->izq), &temp, BO);
              Restructura1(&(nodo), BO);
              free(temp);
            }
          }
        }
      }
    }
  }
  else{
    cout << "El nodo no existe en el arbol" << endl;
    return false;
  }
  *Raiz = nodo;
  return true;
}


int main(){
  int opt;
  int val;
  Nodo *raiz = NULL;
  bool flick = true;
  bool start;
  int change;
  string id;
  string idchange;
  int max;
  int x = 0;
  string line;
  bool check;
  ifstream file("rcsb_pdb_ids-1.txt");
  /* se le pide al usuario que ingrese cuantos valores desea del
     documento de txt*/
  cout << "Cuantos elementos del documento pdb desea agregar" << endl;
  cin >> max;
  if (max > 0) {
    while (x != max) {
      getline(file, line);
      start = false;
      id = '"' + line + '"';
      val = StrToASCII(line);
      addnode(&(raiz), val, &start, id);
      x = x + 1;
    }
  }

  /* Menu donde será infinito hasta que el usuario ingrese 0 */
  while(flick != false){
      cout << "=============================================" << endl;
      cout << "[1] Ingresar nuevo ID" << endl;
      cout << "[2] Eliminar un ID" << endl;
      cout << "[3] Modificar un ID" << endl;
      cout << "[4] Mostrar grafico de arbol" << endl;
      cout << "[0] Para terminar el programa" << endl;
      cout << "=============================================" << endl;
      cout << "Ingrese su opción: " << endl;
      cin >> opt;

      /* Añadir un valor al arbol */
      if (opt == 1) {
        cout << "Ingrese el ID de la proteina que desea añadir" << endl;
        cin >> id;
        val = StrToASCII(id);
        start = false;
        addnode(&(raiz), val, &start, id);
      }

      /* Eliminar un valor al arbol */
      else if (opt == 2){
        cout << "Ingrese el ID que desea eliminar" << endl;
        cin >> id;
        val = StrToASCII(id);
        check = delnode(&(raiz), start, val);
      }

      /* Modificar un valor existente en el arbol */
      else if (opt == 3){
        cout << "Ingrese el ID que desea cambiar" << endl;
        cin >> id;
        val = StrToASCII(id);
        check = delnode(&(raiz), start, val);
        /* si se borra un valor, se puede agregar otro, reemplazandolo */
        if (check == true) {
          cout << "Ingrese a cual ID desea cambiarlo" << endl;
          cin >> idchange;
          change = StrToASCII(idchange);
          addnode(&(raiz), change, &start, idchange);
        }
      }

      /* Graficar el arbol con graphviz */
      else if (opt == 4){
        Grafo *g = new Grafo(raiz);
      }

      /* terminar el programa */
      else if (opt == 0){
        flick = false;
      }

      /* Ingreso de valor inexistente */
      else{
        cout << "Su ingreso es incorrecto, intente de nuevo" << endl;
      }
    }
    return 0;
}
