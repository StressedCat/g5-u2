# G5-U2

Requisitos:
C++
graphviz

Para iniciar el programa:
1 Ingrese en el CMD el comando make para instalar el programa
-> .../g5-u2/Trabajo$ make
2 Se creará un programa con nombre de Cargo, para ingresar a este
haga lo siguiente:
-> .../g5-u2/Trabajo$ ./programa

En el programa:
El programa deberia consistir en un arbol, el cual se va balanceando cada vez que
FE sea mayor a 1 o menor a -1, el cual es capaz de leer un txt para agregar lo que son
proteinas obtenidas de PDB, se pueden agregar, eliminar y modificar

Para borrar el programa:
Asegurese que esté en la misma carpeta donde corrio el programa
-> .../g5-u2/Trabajo
Ingrese el comando make clean para borrar el programa que uso
-> .../g5-u2/Trabajo$ make clean

Notas:
Para ordenar en forma del arbol, las palabras se transforman en un INT por medio de
ASCII, aveces la suma de estos colisiona, asi que no todas las proteinas podrán ser
agregadas para mantener este orden

al graficar da un error, pero los datos son guardados y existe la izq y der, solo
se apunta a si mismo.

FE se actualiza pero no siempre podrá mantener el balance
