#include <fstream>
#include <iostream>
using namespace std;

/* estructura del nodo */
typedef struct _Nodo {
    int info;
    int FE;
    string id;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

/* se crea un nuevo Nodo */
Nodo *crearNodo(int dato, string id) {
    Nodo *q;
    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->id = id;
    q->info = dato;
    q->FE = 0;
    return q;
}
